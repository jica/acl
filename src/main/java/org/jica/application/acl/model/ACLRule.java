package org.jica.application.acl.model;

import java.net.UnknownHostException;

/**
 * Represents an ACL entry.
 * 
 * @author jica
 *
 */
public class ACLRule {
    public static final String WILDCARD = "any";

    private int id;
    private String source;
    private String destination;
    private String protocol;
    private String action;

    public ACLRule() {
    }

    public ACLRule(int id, String source, String destination, String protocol,
	    String action) throws UnknownHostException {
	this.id = id;
	this.source = source;
	this.destination = destination;
	this.protocol = protocol;
	this.action = action;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getSource() {
	return source;
    }

    public void setSource(String source) {
	this.source = source;
    }

    public String getDestination() {
	return destination;
    }

    public void setDestination(String destination) {
	this.destination = destination;
    }

    public String getProtocol() {
	return protocol;
    }

    public void setProtocol(String protocol) {
	this.protocol = protocol;
    }

    public String getAction() {
	return action;
    }

    public void setAction(String action) {
	this.action = action;
    }

    public static String getWildcard() {
	return WILDCARD;
    }

    @Override
    public boolean equals(Object other) {
	if (other == null) {
	    return false;
	}
	if (other == this) {
	    return true;
	}
	if (!(other instanceof ACLRule)) {
	    return false;
	}
	ACLRule otherAclRule = (ACLRule) other;
	return this.id == otherAclRule.getId()
		&& otherAclRule.getSource().equals(this.source)
		&& otherAclRule.getDestination().equals(this.getDestination())
		&& otherAclRule.getProtocol().equals(this.getProtocol())
		&& otherAclRule.getAction().equals(this.getAction());
    }

    @Override
    public String toString() {
	return new StringBuilder().append("ACLRule[").append("id:").append(id)
		.append(",").append("source:").append(source).append(",")
		.append("destination:").append(destination).append(",")
		.append("protocol:").append(protocol).append(",")
		.append("action:").append(action).append("]").toString();
    }
}
