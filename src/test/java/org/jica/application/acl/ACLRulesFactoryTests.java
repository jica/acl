package org.jica.application.acl;

import java.net.UnknownHostException;
import java.util.Iterator;

import org.jica.application.acl.exceptions.InitializationException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.ACLRules;
import org.junit.Assert;
import org.junit.Test;


/**
 * Test the factory that reads the ACL file and creates the ACLRules
 * 
 * @author jica
 *
 */
public class ACLRulesFactoryTests {

    @Test
    public void readDocumentTest() throws InitializationException, UnknownHostException {
	ACLRules rules = ACLRulesFactory.getRules();
	Iterator<ACLRule> it = rules.iterator();
	int index = 1;
	ACLRules expectedRules = createExpectedRules();
	while (it.hasNext()) {
	    Assert.assertEquals(expectedRules.get(index), it.next());
	    index++;
	}
	Assert.assertEquals("Incorrect amount of rules. ",
		expectedRules.size(), index - 1);
    }

    private ACLRules createExpectedRules() throws UnknownHostException {
	ACLRules rules = new ACLRules();

	rules.add(new ACLRule(1, "192.168.0.10", "192.168.0.2", "tcp/80","allow"));
	rules.add(new ACLRule(2, "88.1.12.225", "99.235.1.15", "tcp/80,8080","deny"));
	rules.add(new ACLRule(3, "192.168.0.0/24", "192.168.0.0/28", "udp/any","allow"));
	rules.add(new ACLRule(4, "any", "192.168.0.2", "any", "deny"));
	rules.add(new ACLRule(5, "192.168.0.10", "192.168.0.2", "tcp/80","deny"));
	rules.add(new ACLRule(6, "192.168.0.10/24", "192.168.0.2", "udp/80","allow"));
	rules.add(new ACLRule(7, "any", "192.168.0.2", "tcp/80,88,9090","allow"));
	rules.add(new ACLRule(8, "192.168.0.10", "192.168.0.2","tcp/80,88,9090", "allow"));

	return rules;
    }
}
