package org.jica.application.acl.model;

/**
 * Represents a network packet.
 * @author jica
 *
 */
public class Packet {
    private String source;
    private String destination;
    private String protocol;

    public Packet(){}

    public Packet(String source, String destination, String protocol) {
	this.source = source;
	this.destination = destination;
	this.protocol=protocol;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
	this.protocol=protocol;
    }

    @Override
    public String toString() {
	return new StringBuilder("Package[").append("source:").append(source)
		.append(",destination:").append(destination)
		.append(",protocol:").append(protocol)
		.toString();
    }
}
