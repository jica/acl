package org.jica.application.acl.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * List of ACLRule elements, represents an ACL.
 * 
 * Indexing is counted from 1 to N where N is size()
 * 
 * @author jica
 *
 */
public class ACLRules implements Iterable<ACLRule> {

    private List<ACLRule> aclList = new ArrayList<ACLRule>();

    public boolean add(ACLRule rule) {
	return this.aclList.add(rule);
    }

    @Override
    public Iterator<ACLRule> iterator() {
	return aclList.iterator();
    }

    public int size() {
	return aclList.size();
    }

    /**
     * Returns the index-element of the ACL.
     * 
     * Indexing is counted from 1 to N where N is size().
     * @param index
     * @return 
     */
    public ACLRule get(int index) {
	return aclList.get(index - 1);
    }

    public List<ACLRule> getAclList() {
        return aclList;
    }

    public void setAclList(List<ACLRule> aclList) {
        this.aclList = aclList;
    }
}
