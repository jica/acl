package org.jica.application.acl;

import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.ws.rs.WebApplicationException;

import org.jica.application.acl.ACLResource;
import org.jica.application.acl.exceptions.InitializationException;
import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.ACLRules;
import org.jica.application.acl.model.Packet;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 * Test the service API
 * 
 * @author jica
 *
 */
public class ACLResourceTest {
    private static ACLResource aclList;
    private static ACLRules expectedRules;

    @BeforeClass
    public static void setUp() throws InitializationException,
	    UnknownHostException {
	aclList = new ACLResource();
	expectedRules = new ACLRules();

	expectedRules.add(new ACLRule(1, "192.168.0.10", "192.168.0.2","tcp/80", "allow"));
	expectedRules.add(new ACLRule(2, "88.1.12.225", "99.235.1.15","tcp/80,8080", "deny"));
	expectedRules.add(new ACLRule(3, "192.168.0.0/24", "192.168.0.0/28","udp/any", "allow"));
	expectedRules.add(new ACLRule(4, "any", "192.168.0.2", "any", "deny"));
	expectedRules.add(new ACLRule(5, "192.168.0.10", "192.168.0.2","tcp/80", "deny"));
	expectedRules.add(new ACLRule(6, "192.168.0.10/24", "192.168.0.2","udp/80", "allow"));
	expectedRules.add(new ACLRule(7, "any", "192.168.0.2","tcp/80,88,9090", "allow"));
	expectedRules.add(new ACLRule(8, "192.168.0.10", "192.168.0.2","tcp/80,88,9090", "allow"));
    }

    @Test
    public void getAllRulesTest() throws FileNotFoundException,
	    UnknownHostException {
	ACLRules rules = aclList.getAllRules();
	Iterator<ACLRule> it = rules.iterator();
	int index = 1;
	while (it.hasNext()) {
	    Assert.assertEquals(expectedRules.get(index), it.next());
	    index++;
	}
	Assert.assertEquals("Incorrect amount of rules. ",
		expectedRules.size(), index - 1);
    }

    @Test
    public void getRuleTest() throws UnknownHostException {
	ACLRule expected = expectedRules.get(1);
	ACLRule rule = aclList.getRule(1);
	Assert.assertEquals(expected, rule);
    }

    @Test
    public void getMatchedRule1OKTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(1);
	ACLRule rule = aclList.getMatchedRule(new Packet("192.168.0.10",
		"192.168.0.2", "tcp/80"));
	Assert.assertEquals(expected, rule);
    }

    @Test
    public void getMatchedRule1WrongSourceTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(1);
	ACLRule rule = aclList.getMatchedRule(new Packet("192.168.0.11",
		"192.168.0.2", "tcp/80"));
	Assert.assertNotEquals(expected, rule);
    }

    @Test
    public void getMatchedRule1WrongProtocolTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(1);
	ACLRule rule = aclList.getMatchedRule(new Packet("192.168.0.10",
		"192.168.0.2", "udp/80"));
	Assert.assertNotEquals(expected, rule);
    }

    @Test
    public void getMatchedRule1WrongPortTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(1);
	ACLRule rule = aclList.getMatchedRule(new Packet("192.168.0.10",
		"192.168.0.2", "tcp/9090"));
	Assert.assertNotEquals(expected, rule);
    }

    @Test
    public void getMatchedRule2Test() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(2);
	ACLRule rule = aclList.getMatchedRule(new Packet("88.1.12.225",
		"99.235.1.15", "tcp/80"));
	Assert.assertEquals(expected, rule);

	rule = aclList.getMatchedRule(new Packet("88.1.12.225", "99.235.1.15",
		"tcp/8080"));
	Assert.assertEquals(expected, rule);
    }

    @Test
    public void getMatchedRule3OKTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(3);
	Packet p = new Packet("192.168.0.1", "192.168.0.2", "udp/80");
	ACLRule rule = aclList.getMatchedRule(p);
	Assert.assertEquals(p.toString(), expected, rule);
    }

    @Test
    public void getMatchedRule3WrongSourceTest() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(3);
	Packet p = new Packet("192.168.1.1", "192.168.0.2", "udp/80");
	ACLRule rule = aclList.getMatchedRule(p);
	Assert.assertNotEquals(p.toString(), expected, rule);
    }

    @Test
    public void getMatchedRule3WrongDestinationTest()
	    throws UnknownHostException, MalformedProtocolException {
	Packet p = new Packet("192.168.0.1", "192.168.0.16", "udp/80");
	try {
	    aclList.getMatchedRule(p);
	    Assert.fail("It should return WebApplicationException (404 Error)");
	} catch (WebApplicationException e) {
	}
    }

    @Test
    public void getMatchedRule4Test() throws UnknownHostException,
	    MalformedProtocolException {
	ACLRule expected = expectedRules.get(4);
	ACLRule rule = aclList.getMatchedRule(new Packet("212.73.32.3",
		"192.168.0.2", "udp/80"));
	Assert.assertEquals(expected, rule);
	rule = aclList.getMatchedRule(new Packet("12.73.32.3", "192.168.0.2",
		"udp/80"));
	Assert.assertEquals(expected, rule);
    }
}
