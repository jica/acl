package org.jica.application.acl;

import static org.mockito.Mockito.when;

import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.Packet;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Test the subnetUtilsMatcher class
 * 
 * @author jica
 *
 */
public class SubnetUtilsMatcherTests{

    @Test
    public void matchesAnySourceTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.1","192.168.0.2","tcp/88");
        
        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getDestination()).thenReturn("192.168.0.2");
        when(aclRuleMock.getProtocol()).thenReturn("tcp/80,88,9090");
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertTrue(sum.match(aclRuleMock, p));
    }

    @Test
    public void matchesAnyDestinationTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.1","192.168.0.2","tcp/88");
        
        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn("192.168.1.1");
        when(aclRuleMock.getDestination()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getProtocol()).thenReturn("tcp/80,88,9090");
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertTrue(sum.match(aclRuleMock, p));
    }

    @Test
    public void matchesAnyProtocolTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.1","192.168.0.2","tcp/88");

        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn("192.168.1.1");
        when(aclRuleMock.getDestination()).thenReturn("192.168.0.2");
        when(aclRuleMock.getProtocol()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertTrue(sum.match(aclRuleMock, p));
    }

    @Test
    public void matchesAnyTCPPortTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.1","192.168.0.2","tcp/88");

        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn("192.168.1.1");
        when(aclRuleMock.getDestination()).thenReturn("192.168.0.2");
        when(aclRuleMock.getProtocol()).thenReturn("tcp/"+ACLRule.WILDCARD);
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertTrue(sum.match(aclRuleMock, p));
        
        when(aclRuleMock.getProtocol()).thenReturn("tcp/80");
        Assert.assertFalse(sum.match(aclRuleMock, p));

    }

    @Test
    public void notMatchesSourceTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.2","192.168.0.2","tcp/88");
        
        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn("192.168.1.1");
        when(aclRuleMock.getDestination()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getProtocol()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertFalse(sum.match(aclRuleMock, p));
    }

    @Test
    public void notMatchesDestinationTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.2","192.168.0.2","tcp/88");
        
        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getDestination()).thenReturn("192.168.1.2");
        when(aclRuleMock.getProtocol()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getAction()).thenReturn("allow");

        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertFalse(sum.match(aclRuleMock, p));
    }

    @Test
    public void notMatchesProtocolTest() throws MalformedProtocolException {
        Packet p = new Packet("192.168.1.2","192.168.0.2","tcp/88");

        ACLRule aclRuleMock = Mockito.mock(ACLRule.class);
        when(aclRuleMock.getId()).thenReturn(1);
        when(aclRuleMock.getSource()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getDestination()).thenReturn(ACLRule.WILDCARD);
        when(aclRuleMock.getProtocol()).thenReturn("udp/88");
        when(aclRuleMock.getAction()).thenReturn("allow");


        SubnetUtilsMatcher sum = new SubnetUtilsMatcher();
        Assert.assertFalse(sum.match(aclRuleMock, p));

        when(aclRuleMock.getProtocol()).thenReturn("udp/8888");
        Assert.assertFalse(sum.match(aclRuleMock, p));
    }

}
