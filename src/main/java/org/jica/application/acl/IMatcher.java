package org.jica.application.acl;

import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.Packet;

/**
 * Defines the operations to match an ACLRule with a specific packet
 * 
 * @author jica
 *
 */
public interface IMatcher {
    /**
     * Check if an ACLRule matches with a specific packet
     * @param aclRule
     * @param p
     * @return
     * @throws MalformedProtocolException
     */
    public boolean match(ACLRule aclRule, Packet p)
	    throws MalformedProtocolException;
}