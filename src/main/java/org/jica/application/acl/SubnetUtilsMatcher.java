package org.jica.application.acl;

import java.util.Arrays;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.log4j.Logger;
import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.Packet;

/**
 * Uses org.apache.commons.net.util.SubnetUtils to match ACLRule with an
 * specific packet.
 * 
 * The mask /0 is not allowed so it is necessary a workaround to match 'any'
 * address.
 * 
 * @author jica
 *
 */
public class SubnetUtilsMatcher implements IMatcher {

    private static final Logger log = Logger.getLogger(SubnetUtils.class);

    @Override
    public boolean match(ACLRule aclRule, Packet p)
	    throws MalformedProtocolException {
	log.debug("match(" + aclRule + "," + p + ")");
	return matchNetwork(aclRule.getSource(), p.getSource())
		&& matchNetwork(aclRule.getDestination(), p.getDestination())
		&& matchProtocolAndPort(aclRule.getProtocol(), p.getProtocol());

	// boolean matchSource = matchNetwork(aclRule.getSource(),
	// p.getSource());
	// boolean matchDestination = matchNetwork(aclRule.getDestination(),
	// p.getDestination());
	// boolean matchProtocolAndPort = matchProtocolAndPort(
	// aclRule.getProtocol(), p.getProtocol());
	// return matchSource && matchDestination && matchProtocolAndPort;
    }

    private boolean matchNetwork(String subnet, String address) {
	log.debug("matchNetwork(" + subnet + "," + address + ")");
	if (ACLRule.WILDCARD.equals(subnet)) {
	    return true;
	}
	SubnetUtils subnetUtil;
	if (subnet.contains("/")) {
	    subnetUtil = new SubnetUtils(subnet);
	} else {
	    subnetUtil = new SubnetUtils(subnet + "/32");
	}
	subnetUtil.setInclusiveHostCount(true);
	return subnetUtil.getInfo().isInRange(address);
    }

    private boolean matchProtocolAndPort(String netProtocol, String packProtocol) {
	log.debug("matchProtocolAndPort(" + netProtocol + "," + packProtocol
		+ ")");
	if (ACLRule.WILDCARD.equals(netProtocol)) {
	    return true;
	}
	String[] netProtocolAux = netProtocol.split("/");
	String[] addrProtocolAux = packProtocol.split("/");
	return matchProtocol(netProtocolAux[0], addrProtocolAux[0])
		&& matchPort(netProtocolAux[1], addrProtocolAux[1]);
    }

    private boolean matchProtocol(String netProtocol, String packProtocol) {
	log.debug("matchProtocol(" + netProtocol + "," + packProtocol + ")");
	if (ACLRule.WILDCARD.equals(netProtocol)) {
	    return true;
	}
	return netProtocol.equals(packProtocol);
    }

    private boolean matchPort(String netPorts, String packPort) {
	log.debug("matchPort(" + netPorts + "," + packPort + ")");
	if (ACLRule.WILDCARD.equals(netPorts)) {
	    return true;
	}
	String[] ports = netPorts.split(",");
	Arrays.sort(ports);
	return Arrays.binarySearch(ports, packPort) >= 0;
    }
}
