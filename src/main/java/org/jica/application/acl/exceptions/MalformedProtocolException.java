package org.jica.application.acl.exceptions;

public class MalformedProtocolException extends Exception {

    public MalformedProtocolException(String string) {
	super(string);
    }

}
