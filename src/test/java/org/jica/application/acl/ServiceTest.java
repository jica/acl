package org.jica.application.acl;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.net.UnknownHostException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.ACLRules;
import org.jica.application.acl.model.Packet;
import org.junit.Test;

/**
 * Integration test for the REST API
 * 
 * @author jica
 *
 */
public class ServiceTest extends JerseyTest {
    @Override
    public Application configure() {
	enable(TestProperties.LOG_TRAFFIC);
	enable(TestProperties.DUMP_ENTITY);

	return new ResourceConfig(ACLResource.class);
    }

    @Test
    public void getWrongPathTest() {
	Response output = target("/wrongPath").request().get();
	assertEquals("should return status 200", false,
		200 == output.getStatus());
    }

    @Test
    public void getAllRulesTest() {
	ACLRules output = target("/acl").request().get(ACLRules.class);
	assertEquals(8, output.size());
    }

    @Test
    public void getNotExistingRuleTest() {
	Response output = target("/acl/9999").request().get();
	assertEquals("should return status 404", 404, output.getStatus());
    }

    @Test
    public void getRuleTest() throws UnknownHostException {
	ACLRule expected = new ACLRule(1, "192.168.0.10", "192.168.0.2",
		"tcp/80", "allow");
	ACLRule output = target("/acl/1").request().get(ACLRule.class);
	assertEquals(expected, output);
    }

    @Test
    public void postMatchedRuleTest() throws UnknownHostException {
	Packet packet = new Packet("192.168.0.10", "192.168.0.2", "tcp/80");
	Response output = target("/acl").request().post(
		Entity.entity(packet, MediaType.APPLICATION_JSON));

	assertEquals("Should return status 200", 200, output.getStatus());
	assertNotNull("Should return notification", output.getEntity());
    }

    @Test
    public void getMatchedRuleTest() throws UnknownHostException,
	    MalformedProtocolException {
	Response output = target("/acl/match")
		.queryParam("source", "192.168.0.10")
		.queryParam("destination", "192.168.0.2")
		.queryParam("protocol", "tcp/80").request().get();
	assertEquals("should return status 200", 200, output.getStatus());
	assertNotNull("Should return ACLRule", output.getEntity());
    }
}
