package org.jica.application.acl;

import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.jica.application.acl.exceptions.InitializationException;
import org.jica.application.acl.exceptions.MalformedProtocolException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.ACLRules;
import org.jica.application.acl.model.Packet;

/**
 * Exposes a REST API to operates with ACL.
 * 
 * @author jica
 *
 */
@Path("acl")
public class ACLResource extends Application {
    private static final Logger log = Logger.getLogger(ACLResource.class);
    private static ACLRules rules;

    private IMatcher matcher;

    public ACLResource() throws InitializationException {
	log.debug("ACLResource created");
	if (rules == null) {
	    log.debug("rules is null -> invoke the factory to fill from file");
	    rules = ACLRulesFactory.getRules();
	}
	matcher = new SubnetUtilsMatcher();
    }

    /**
     * Returns the whole ACL
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ACLRules getAllRules() {
	log.debug("getAllRules()");
	log.debug("return: " + rules + " with " + rules.size() + " elements.");
	return rules;
    }

    /**
     * Get a single rule.
     * 
     * @param id
     *            The index of the rule on the list. Indexing is counted from 1
     *            to size()
     * @return The rule on @param position
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ACLRule getRule(@PathParam("id") int id) {
	log.debug("getRule(" + id + ")");
	ACLRule res;
	try {
	    res = rules.get(id);
	} catch (IndexOutOfBoundsException e) {
	    log.info("Requested not existing rule: " + id);
	    throw new WebApplicationException("Not exists rule " + id, 404);
	}
	log.debug("returns: " + res);
	return res;
    }

    /**
     * Return the first rule that their fields match with the packet fields in
     * order to know what action to apply
     * 
     * @param p The packet to check
     * @return The ACLRule that matches the packet or HTTP_404 if there are no rules
     * @throws UnknownHostException
     * @throws MalformedProtocolException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ACLRule getMatchedRule(Packet p) throws UnknownHostException,
	    MalformedProtocolException {
	log.debug("getMarchedRule(" + p + ")");
	ACLRule res = null;
	for (ACLRule aclRule : rules) {
	    if (matcher.match(aclRule, p)) {
		// if (aclRule.matches(p)) {
		log.debug("Matched!: " + aclRule);
		res = aclRule;
		break;
	    }
	}
	log.debug("returns: " + res);
	if(res == null){
	    throw new WebApplicationException("There are no rules that matches "+p,404);
	}
	return res;
    }

    /**
     * Return the first rule that their fields match with the packet fields in
     * order to know what action to apply
     * 
     * @param source of the packet to check
     * @param destination of the packet to check
     * @param protocol of the packet to check
     * @return The ACLRule that matches the packet or HTTP_404 if there are no rules
     * @throws UnknownHostException
     * @throws MalformedProtocolException
     */
    @GET
    @Path("/match")
    @Produces(MediaType.APPLICATION_JSON)
    public ACLRule getMatchedRule(@QueryParam("source") String source,
	    @QueryParam("destination") String destination,
	    @QueryParam("protocol") String protocol)
	    throws UnknownHostException, MalformedProtocolException {
	log.debug("getMatchedRule(" + source + "," + destination + ","
		+ protocol + ")");
	Packet p = new Packet(source, destination, protocol);
	ACLRule res = null;
	for (ACLRule aclRule : rules) {
	    if (matcher.match(aclRule, p)) {
		// if (aclRule.matches(p)) {
		log.debug("Matched!: " + aclRule);
		res = aclRule;
		break;
	    }
	}
	log.debug("returns: " + res);
	if(res == null){
	    throw new WebApplicationException("There are no rules that matches "+p,404);
	}
	return res;
    }
}
