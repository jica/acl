package org.jica.application.acl.exceptions;

import java.io.IOException;

public class InitializationException extends Exception {

    public InitializationException(String string, IOException e) {
	super(string,e);
    }

}
