package org.jica.application.acl;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jica.application.acl.exceptions.InitializationException;
import org.jica.application.acl.model.ACLRule;
import org.jica.application.acl.model.ACLRules;

/**
 * Factory that creates ACLRules objects
 * 
 * @author jica
 *
 */
public class ACLRulesFactory {
    private static final Logger log = Logger.getLogger(ACLRulesFactory.class);
    public static final String ACL_FILENAME = "ACL.FILEPATH";
    public static final String DEFAULT_ACLFILE = "acl.txt";

    private static final String CIDR = "(?:\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}(?:/\\d{1,3})?)";
    private static final String PROTOCOL = "(?:tcp|udp)";
    private static final String PORTS = "(?:(?:\\d+(?:[,]\\d+)*)|"
	    + ACLRule.WILDCARD + ")";
    private static final String PROTOCOL_PORTS = PROTOCOL + "/" + PORTS;
    private static final String RULE = "(\\d+) from (" + CIDR + "|"
	    + ACLRule.WILDCARD + ") to (" + CIDR + "|" + ACLRule.WILDCARD
	    + ") with (" + PROTOCOL_PORTS + "|any) => (allow|deny)";
    private static final Pattern RULE_PATTERN = Pattern.compile(RULE);

    private static final int INDEX_ID = 1;
    private static final int INDEX_SOURCEIP = 2;
    private static final int INDEX_DESTINATIONIP = 3;
    private static final int INDEX_PROTOCOL = 4;
    private static final int INDEX_ACTION = 5;

    private static ACLRules rules = null;

    public static synchronized ACLRules getRules() throws InitializationException {
	if (rules == null) {
	    String aclFileName = System.getProperty(ACL_FILENAME);
	    try {
		if (aclFileName == null) {
		    aclFileName = DEFAULT_ACLFILE;
		}
		rules = createFromFile(aclFileName);
	    } catch (FileNotFoundException | UnknownHostException e) {
		log.error("There was an error loading ACL file " + aclFileName,
			e);
		throw new InitializationException(
			"There was an error loading ACL file " + aclFileName, e);
	    }
	}
	return rules;
    }

    /**
     * Creates ACLRules object from a file the firs time it is executed and save
     * the ACLRules object as static.
     * 
     * ACL file must be correct.
     * 
     * @param filename
     * @return ACLRules object with all the rules
     * @throws FileNotFoundException
     * @throws UnknownHostException
     */
    private static ACLRules createFromFile(String filename)
	    throws FileNotFoundException, UnknownHostException {
	log.debug("Creating ACLRules from " + filename);
	ACLRules result = new ACLRules();
	File f = new File(ACLRules.class.getClassLoader().getResource(filename)
		.getFile());

	Scanner scan = null;
	try {
	    scan = new Scanner(f);
	    MatchResult values;
	    while (scan.hasNextLine()) {
		scan.findInLine(RULE_PATTERN);
		values = scan.match();
		int ruleId = Integer.valueOf(values.group(INDEX_ID));
		ACLRule rule = new ACLRule(ruleId,
			values.group(INDEX_SOURCEIP),
			values.group(INDEX_DESTINATIONIP),
			values.group(INDEX_PROTOCOL),
			values.group(INDEX_ACTION));
		result.add(rule);
		log.debug("  Add: " + rule);
		try {
		    scan.nextLine();
		} catch (NoSuchElementException e) {
		    log.debug("There are no more lines on " + filename);
		}
	    }
	} finally {
	    if (scan != null) {
		scan.close();
	    }
	}
	return result;
    }
}
