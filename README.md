# Access Control List

The access control policies allow defining a ruleset that determines whether a source endpoint can establish a network communication or not with a destination endpoint within a network infrastructure. A policy is composed of an access control list (ACL) consisting in a ruleset that allows or denies traffic between two nodes for a specific protocol and ports. Each network element is represented by using its IP address with CIDR (for example 192.168.10.0/24), or using wildcard operator (for example “any”).

These ACLs are implemented in a variety of network devices, being the firewalls the most relevant special-purpose devices in a network security implementation. Below an instance of an ACL where the wildcard operator is implemented with the keyword
“any”:

```
Source         Destination    Protocols/Ports    Action
192.168.0.10   192.168.0.2    TCP/80             ALLOW
88.1.12.225    99.235.1.15    TCP/80,8080        DENY
192.168.0.0/24 192.168.0.0/28 UDP/any            ALLOW
any            192.168.0.2    any                DENY
```

Once an IP packet traverses the firewall, the matching algorithm evaluates every ACL rule from top to down until a single rule is matched with the packet fields and the firewall will execute the action for matched rule.

##System specification
Given a comprehensive ACL in an external file with the following format:
```
1 from 192.168.0.10 to 192.168.0.2 with tcp/80 => allow
2 from 88.1.12.225 to 99.235.1.15 with tcp/80,8080 => deny
3 from 192.168.0.0/24 to 192.168.0.0/28 with udp/any => allow
4 from any to 192.168.0.2 with any => deny
```
The system must offer a REST API to operate with the Access Control List. The allowed operations are:

- Get the ACL
  - Description: Returns the whole ACL
  - Endpoint: /intelliment/acl
  - Operation: GET

- Get a single rule
  - Description: Returns the single rule given the id
  - Endpoint: /intelliment/acl/:id
  - Operation: GET
  - **Considerations:** 
    - In the provided example, the id corresponds with the line number. 
    - Also, the priority of a rule, when the system is looking for the rule that matches a packet, is defined by the order in wich appears on the ACL file.
    - In the solution, this method returns the rule on the line *id*. I have maintained the id as a 'numeric descriptor' of the rule to be easier to identify it.
    - To use it as an identifier and be able to get the element identified by that field, could be created a hashmap to link the id with the corresponding rule.

- Get the matched rule for an specific packet
  - Description: Return the first rule that their fields match with the packet fields in order to know what action to apply. For example, for a packet with fields: [source=”192.168.0.5”, destination=”192.168.0.1” and protocol=”UDP/80”] the first rule that match it will be the number 3.
  - Endpoint: /intelliment/acl
  - Operation: GET
  - **Considerations:** 
    - The operation is requested to be GET and consumes a JSON with the packet to be checked, but, according to the main answer to this [StackOverflow question](http://stackoverflow.com/questions/978061/http-get-with-request-body?noredirect=1&lq=1), "the request-body is not part of the identification of the resource in a GET request, only the request URI".
    - I have developed 2 operations:
      1. Operation: POST
        - Endpoint: /intelliment/acl
        - Description: The same but a POST operation (The packet is sent as JSON parameter)
      2. Operation: GET
        - Endpoint: /intelliment/acl/match?source=SOURCE_IP&destination=DESTINATION_IP&protocol=PROTOCOL/PORT
        - Description: A new endpoint is needed because it has conflict with the first operation.

##Other considerations (and some TODO)
- Matcher implementation:
  - Use of SubnetUtils (org.apache.commons.net.util.SubnetUtils) to match subnet with the addresses.
  - SubnetUtils does not allow /0 to match 'any' address.
    - to resolve that, in the case that the subnet string is 'any', the matcher returns true, even without parsing the address.
    - Other workaround: Using list of SubnetUtils with: *0.255.255.255/1* and *255.255.255.255/1*
  - TODO: The solution creates the SubnetUtils object for every rule each time it is needed. It should be better to refactor the code in order to create the SubnetUtil object only one time for each rule(may be on the Constructor).
  - TODO: Create other implementation of IMatcher that manually matches subnets and addresses.

- ACLRule
Restrictions about allowed actions (allow|deny) or protocols (udp|tcp) are not on ACLRule. I've prefered to parse each line only one time, instead of parse one time to get the ACLRule elements (delimited by from, to, with, =>) and one more time to parse subnet addresses, protocol, ... So you can set any string on the fields of the ACLRule, but it will fail when the matcher use it.
  
- ACLRules
  - Basically a list of ACLRule elements, represents an ACL.
  - Indexing is counted from 1 to N where N is size().

##Tests
To execute the tests: `mvn test`
For the MatcherTests I have used Mockito to emulates ACLRule.

##Execution
Run with `mvn jetty:run`
From a REST client (RESTClient plugin for Firefox):

- Get the ACL
  - Method: GET
  - URL: http://localhost:8080/intelliment/acl
- Get a specific rule (second on the list)
  - Method: GET
  - URL: http://localhost:8080/intelliment/acl/2
- Get the  rule that matches a packet (source=108.12.47.121,destination=108.12.47.120,protocol=udp/44810)
  - Method: GET
    - URL: http://localhost:8080/intelliment/acl/match?source=108.12.47.121&destination=108.12.47.120&protocol=udp/44810
  - Method: POST
    - URL: http://localhost:8080/intelliment/acl
    - Header: Content-Type: application/json
    - Body: 
```
{
    "source": "108.12.47.121",
    "destination": "108.12.47.120",
    "protocol": "udp/44810"
}
```

Para indicar un fichero ACL distinto, copiar el fichero en src/main/resources y establecer la propiedad ACL.FILEPATH con el nombre de dicho fichero. Por ejemplo, si el fichero se llama acl_test.txt:
`mvn jetty:run -DACL.FILEPATH="acl_test.txt"`
